package cn.edu.njau.qing.algo;

import java.util.ArrayList;

import cn.edu.njau.qing.Qing;

//情感分析算法
public class QingAlgo {

	private String content;
	private String[][] fmWord;
	private String[][] zmWord;
	private String[][] ztWord;
	private String[][] zyWord;
	private int fmLen;
	private int zmLen;
	private int ztLen;
	private int zyLen;

	public QingAlgo(String content) {
		this.content = content;
		this.fmWord = Qing.fmWord;
		this.zmWord = Qing.zmWord;
		this.ztWord = Qing.ztWord;
		this.zyWord = Qing.zyWord;
		fmLen = fmWord.length;
		zmLen = zmWord.length;
		ztLen = ztWord.length;
		zyLen = zyWord.length;
	}

	// 计算一篇新闻的情感值
	public int getNewsQingValue(int id) {

		ArrayList<QingValue> qtmp = new ArrayList<QingValue>();

		// 先对网页内容进行切分句子处理，切分好的句子放在stmp中
		SentSegAlgo ssa = new SentSegAlgo();
		String[] stmp = ssa.split(content);
		int sLen = stmp.length;
		int vtmp = 0;
		
		boolean flag = true;
		for (int i = 0; i < sLen; i++) {

			// 句子处理
			// 第一步，判断是否为主观句
			if (isSubject(stmp[i])) {
				System.out.println("找到情感句！___" + stmp[i]);

				// 第二步，计算情感值
				QingValue qa = getSentQingValue(id, stmp[i] , flag);
				flag = false;
				System.out.println("此句的来源为：" + qa.qValue);
				System.out.println("此句的情感值为：" + qa.qValue);
				Qing.aqv.add(qa);// 保存处理结果
				qtmp.add(qa);

			} else {
				System.out.println("不是情感句！___" + stmp[i]);
			}
		}

		// 判断一篇新闻的情感值
		int qLen = qtmp.size();
		for (int j = 0; j < qLen; j++) {
			vtmp = vtmp + qtmp.get(j).qValue;
		}

		// 根据主题词类别来判断
		int zcLen = Qing.ztClass.length;
		int[] cValue = new int[zcLen];
		for (int k = 0; k < zcLen; k++) {
			cValue[k] = 0;
		}
		for (int j = 0; j < qLen; j++) {
			for (int k = 0; k < zcLen; k++) {
				if (Qing.ztClass[k].equals(qtmp.get(j).ztFrom)) {
					cValue[k]++;
				}
			}
		}
		Qing.ai.add(cValue);

		// 记录每条新闻情感值日志
		Qing.log.addLog("");
		Qing.log.addLog("--------------------");
		Qing.log.addLog("新闻ID：" + id);
		Qing.log.addLog("情感值：" + vtmp);
		Qing.gname.add(Integer.toString(id));
		Qing.gvalue.add(Integer.toString(vtmp));

		return vtmp;
	}

	// 判断情感句
	private boolean isSubject(String s) {

		boolean flag = false;

		for (int i = 0; i < ztLen; i++) {
			if (s.indexOf(ztWord[i][0]) >= 0) {
				flag = true;
			}
		}

		if (!flag) {
			for (int i = 0; i < fmLen; i++) {
				if (s.indexOf(fmWord[i][0]) >= 0) {
					flag = true;
				}
			}
		}

		if (!flag) {
			for (int i = 0; i < zmLen; i++) {
				if (s.indexOf(zmWord[i][0]) >= 0) {
					flag = true;
				}
			}
		}
		return flag;
	}

	// 计算一个句子的情感值
	public QingValue getSentQingValue(int id, String s, boolean log) {

		if (log) {
			Qing.logw.addLog("新闻ID：" + id);
			Qing.logzt.addLog("新闻ID：" + id);
			Qing.logw.addLog("");
			Qing.logzt.addLog("");
		}
		ArrayList<String> zt = new ArrayList<String>();
		ArrayList<String> cs = new ArrayList<String>();
		// 取出里面的主题词
		for (int i = 0; i < ztLen; i++) {
			if (s.indexOf(ztWord[i][0]) >= 0) {
				zt.add(ztWord[i][0]);
				cs.add(ztWord[i][1]);
				if (log) {
					Qing.logzt.addLog(ztWord[i][0]);
				}
			}
		}

		ArrayList<String> fm = new ArrayList<String>();
		ArrayList<Integer> fmValue = new ArrayList<Integer>();
		ArrayList<Integer> fmBegin = new ArrayList<Integer>();
		// 取出里面的负面情感词
		for (int i = 0; i < fmLen; i++) {
			if (s.indexOf(fmWord[i][0]) >= 0) {
				fm.add(fmWord[i][0]);
				fmValue.add(Integer.parseInt(fmWord[i][1]));
				fmBegin.add(s.indexOf(fmWord[i][0]));
				if (log) {
					Qing.logw.addLog(fmWord[i][0]);
				}
			}
		}

		ArrayList<String> zm = new ArrayList<String>();
		ArrayList<Integer> zmValue = new ArrayList<Integer>();
		ArrayList<Integer> zmBegin = new ArrayList<Integer>();
		// 取出里面的正面情感词
		for (int i = 0; i < zmLen; i++) {
			if (s.indexOf(zmWord[i][0]) >= 0) {
				zm.add(zmWord[i][0]);
				zmValue.add(Integer.parseInt(zmWord[i][1]));
				zmBegin.add(s.indexOf(zmWord[i][0]));
				if (log) {
					Qing.logw.addLog(zmWord[i][0]);
				}
			}
		}

		ArrayList<String> zy = new ArrayList<String>();
		ArrayList<Integer> zyValue = new ArrayList<Integer>();
		ArrayList<Integer> zyBegin = new ArrayList<Integer>();
		// 取出里面的转义词
		for (int i = 0; i < zyLen; i++) {
			if (s.indexOf(zyWord[i][0]) >= 0) {
				zy.add(zyWord[i][0]);
				zyValue.add(Integer.parseInt(zyWord[i][1]));
				zyBegin.add(s.indexOf(zyWord[i][0]));
			}
		}

		// 计算情感值
		QingValue qa = new QingValue();
		int qZtLen = zt.size();// 主题词数量
		int qFmLen = fm.size();// 句子中负面情感词数量
		int qZmLen = zm.size();// 句子中正面情感词数量
		int qZyLen = zy.size();// 句子中转义词数量

		for (int i = 0; i < qFmLen; i++) {
			qa.qValue = qa.qValue + fmValue.get(i);
		}

		for (int i = 0; i < qZmLen; i++) {
			qa.qValue = qa.qValue + zmValue.get(i);
		}

		for (int i = 0; i < qZyLen; i++) {
			qa.qValue = qa.qValue * zyValue.get(i);
		}

		if (qZtLen == 1) {
			qa.ztFrom = zt.get(0);
		} else {
			qa.ztFrom = "其他";
		}

		if (log) {
			Qing.logw.addLog("------------");
			Qing.logzt.addLog("-----------");
		}
		return qa;
	}

}
