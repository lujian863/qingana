package cn.edu.njau.qing.algo;

//切分句子
public class SentSegAlgo {
	
	String[] fuhao = {"。", "！", "？", "!", "?", "\n"};
	String reg = "";
	
	public SentSegAlgo() {
		// 从conf/juzi.xml文件中读取切分句子的标识
		// ...
		int fLen = fuhao.length;	
		for (int i = 0; i < fLen; i++) {
			//reg = reg + "|(" + fuhao[i] + ")";// 构造正则
			reg = reg + "|[" + fuhao[i] + "]";// 构造正则
		}
		reg = reg.substring(1);// 去掉第一个 | 符号
		//System.out.println("正则表达式为："+reg);
	}

	// 切分句子
	public String[] split(String s) {
		return s.split(reg);
	}
}
