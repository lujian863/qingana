package cn.edu.njau.qing.algo;

public class QingValue {
	
	//这个类的对象是一个句子情感值的返回结果
	
	public QingValue(){
		nid = 0;
		ztFrom = "";
		qValue = 0;
	}	
	
	public int nid;//新闻的ID
	public String ztFrom;//主题来源（政府、媒体等）
	public int qValue;//句子的情感值
	
}
