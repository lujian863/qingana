package cn.edu.njau.qing;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import cn.edu.njau.qing.algo.QingAlgo;
import cn.edu.njau.qing.algo.QingValue;
import cn.edu.njau.qing.util.MyCSVReader;
import cn.edu.njau.qing.util.MyCSVWriter;
import cn.edu.njau.qing.util.MyDBConn;
import cn.edu.njau.qing.util.MyLogWriter;
import cn.edu.njau.qing.util.MyXMLReader;

public class Qing {

	private static String dbConfPath = "conf/db.xml";

	private static String sqlLen = "select count(*) from tb_data";
	private static String sqlCon = "select * from tb_data";

	private static String fmWordPath = "conf/负面情感词.csv";
	private static String zmWordPath = "conf/正面情感词.csv";
	private static String ztWordPath = "conf/主题词.csv";
	private static String zyWordPath = "conf/转义词.csv";

	public static String[][] fmWord = null;
	public static String[][] zmWord = null;
	public static String[][] ztWord = null;
	public static String[][] zyWord = null;

	private static String host = "";
	private static String port = "";
	private static String dbname = "";
	private static String username = "";
	private static String password = "";

	static MyDBConn dbc;
	static Connection conn;

	// 保存分析结果
	public static ArrayList<QingValue> aqv = new ArrayList<QingValue>();
	public static ArrayList<int[]> ai = new ArrayList<int[]>();

	public static String[] ztClass = null;

	// 日志相关的
	public static MyLogWriter log = null;
	public static MyLogWriter logw = null;
	public static MyLogWriter logzt = null;
	public static ArrayList<String> gname = null;
	public static ArrayList<String> gvalue = null;

	// 程序入口
	public static void main(String[] args) {

		SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
		String logNews = "logs/log_新闻_" + sm.format(new Date()).toString()
				+ ".log";
		String logWord = "logs/log_情感词_" + sm.format(new Date()).toString()
				+ ".log";
		String logZt = "logs/log_主题词_" + sm.format(new Date()).toString()
				+ ".log";
		String csvName = "csvs/新闻ID-情感值_" + sm.format(new Date()).toString()
				+ ".csv";

		// 实例化写日志
		log = new MyLogWriter(logNews);
		logw = new MyLogWriter(logWord);
		logzt = new MyLogWriter(logZt);
		gname = new ArrayList<String>();
		gvalue = new ArrayList<String>();

		// 首先载入词库
		MyCSVReader mcr = new MyCSVReader(fmWordPath);
		fmWord = mcr.getStCon();// 读取负面情感词库
		System.out.println("已载入_" + fmWordPath + "_词库！");

		mcr = new MyCSVReader(zmWordPath);
		zmWord = mcr.getStCon();// 读取正面情感词库
		System.out.println("已载入_" + zmWordPath + "_词库！");

		mcr = new MyCSVReader(ztWordPath);
		ztWord = mcr.getStCon();// 读取主题词库
		System.out.println("已载入_" + ztWordPath + "_词库！");

		mcr = new MyCSVReader(zyWordPath);
		zyWord = mcr.getStCon();// 读取转义词库
		System.out.println("已载入_" + zyWordPath + "_词库！");

		// 获取主题词类别
		int ztLen = ztWord.length;
		ztClass = new String[ztLen];
		for (int x = 0; x < ztLen; x++) {
			ztClass[x] = ztWord[x][1];
		}
		ztClass = getUniqueHashSet(ztClass);// 去除重复的类别

		// 读取数据库连接信息
		MyXMLReader mxr = new MyXMLReader(dbConfPath);
		if (mxr.DBConf != null) {
			System.out.println("正在读取数据库配置信息...");
		} else {
			System.out.println("数据库配置信息读取失败！请检查数据库连接配置信息！");
		}
		host = mxr.DBConf[0][0];
		port = mxr.DBConf[0][1];
		dbname = mxr.DBConf[0][2];
		username = mxr.DBConf[0][3];
		password = mxr.DBConf[0][4];
		sqlLen = "select count(*) from " + mxr.DBConf[0][5];
		sqlCon = "select " + mxr.DBConf[0][6] + " from " + mxr.DBConf[0][5];
		System.out.println("已完成数据库配置信息读取！");
		// System.out.println(sqlLen);

		// 建立数据库连接
		System.out.println("正在连接数据库...");
		dbc = new MyDBConn(host, port, dbname, username, password);
		try {
			conn = dbc.getConn();
			System.out.println("数据库连接成功！");
		} catch (SQLException e) {
			System.out.println("数据库连接失败！");
			e.printStackTrace();
		}

		// 读取新闻内容
		Statement stmt;
		String[][] newsCon = null;
		try {
			stmt = conn.createStatement();
			ResultSet rs_len = stmt.executeQuery(sqlLen);
			rs_len.next();
			int len = rs_len.getInt(1);
			rs_len.close();
			newsCon = new String[len][3];
			ResultSet rs = stmt.executeQuery(sqlCon);
			int i = 0;
			System.out.println("正在读取网页内容...");
			while (rs.next()) {
				newsCon[i][0] = rs.getString(mxr.DBConf[0][6]);
				newsCon[i][1] = "";// 存放该篇新闻的情感分析值
				newsCon[i][2] = "";// 存放来源(暂时没有用上)
				i++;
			}
			rs.close();
			stmt.close();
			conn.close();
			System.out.println("网页内容读取成功！");
		} catch (SQLException e) {
			System.out.println("网页内容读取异常！");
			e.printStackTrace();
		}

		// 开始处理新闻
		int nLen = newsCon.length;// 获取新闻总数目

		// 一条条开始处理
		for (int i = 0; i < nLen; i++) {
			QingAlgo qa = new QingAlgo(newsCon[i][0]);
			newsCon[i][1] = Integer.toString(qa.getNewsQingValue(i));// 传进去的i视为新闻的ID
		}

		// 统计结果
		System.out.println("******************************************");
		System.out.println("下面是统计结果：");
		System.out.println("");
		log.addLog("******************************************");
		log.addLog("下面是统计结果：");
		log.addLog("");

		int zNum = 0;
		int fNum = 0;
		int xNum = 0;
		for (int i = 0; i < nLen; i++) {
			if (Integer.parseInt(newsCon[i][1]) == 0) {
				xNum++;
			} else if (Integer.parseInt(newsCon[i][1]) > 0) {
				zNum++;
			} else {
				fNum++;
			}
		}
		System.out.println("中性新闻条数为：" + xNum);
		System.out.println("正面新闻条数为：" + zNum);
		System.out.println("负面新闻条数为：" + fNum);
		log.addLog("中性新闻条数为：" + xNum);
		log.addLog("正面新闻条数为：" + zNum);
		log.addLog("负面新闻条数为：" + fNum);

		// 分类统计
		System.out.println("");
		System.out.println("分类统计结果：");
		log.addLog("");
		log.addLog("分类统计结果：");
		for (int i = 0; i < ztClass.length; i++) {
			System.out.println("-----------------------------");
			System.out.println(ztClass[i] + "__类别：");
			log.addLog("-----------------------------");
			log.addLog(ztClass[i] + "__类别：");

			int za = 0;// 正
			int zb = 0;// 中
			int zc = 0;// 负
			for (int j = 0; j < nLen; j++) {
				int[] tmp = ai.get(j);
				if (tmp[i] > 0) {
					za++;
				} else if (tmp[i] == 0) {
					zb++;
				} else {
					zc++;
				}
			}
			System.out.println("正面： " + za + " 条");
			System.out.println("中性： " + zb + " 条");
			System.out.println("负面： " + zc + " 条");
			log.addLog("正面： " + za + " 条");
			log.addLog("中性： " + zb + " 条");
			log.addLog("负面： " + zc + " 条");
		}

		System.out.println("");
		System.out.println("");
		System.out.println("程序版本：QingAna v1.1");
		System.out.println("更新日期：2012年7月19日");
		log.addLog("");
		log.addLog("");
		log.addLog("程序版本：QingAna v1.1");
		log.addLog("更新日期：2012年7月19日");

		log.closeLog();
		logw.closeLog();
		logzt.closeLog();
		System.out.println("");
		System.out.println("");
		System.out.println("日志文件--- " + logNews + "---已成功生成！");
		System.out.println("日志文件---" + logWord + "---已成功生成！");
		System.out.println("日志文件---" + logZt + "---已成功生成！");

		// 输出csv文件
		int gLen = gname.size();
		String[][] arr = new String[gLen + 1][2];
		arr[0][0] = "新闻ID";
		arr[0][1] = "情感值";
		for (int i = 0; i < gLen; i++) {
			arr[i][0] = gname.get(i);
			arr[i][1] = gvalue.get(i);
		}
		try {
			MyCSVWriter.writeCVS(arr, csvName);
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("CSV文件---" + csvName + "---已成功生成！");
	}

	// 取出数组中重复的元素
	private static String[] getUniqueHashSet(String[] data) {
		Set<String> set = new HashSet<String>(Arrays.asList(data));
		String[] oArray = set.toArray(new String[] {});
		return oArray;
	}

}
