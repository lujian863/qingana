package cn.edu.njau.qing.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MyLogWriter {

	// 打印日志类
	File log = null;
	FileWriter fileWriter = null;
	BufferedWriter bw = null;
	
	public MyLogWriter(String fname) {
		log = new File(fname);
		try {
			fileWriter = new FileWriter(log);
		} catch (IOException e) {
			e.printStackTrace();
		}
		bw = new BufferedWriter(fileWriter);
		System.out.println("日志文件 " + fname + " 创建成功！");
	}

	public void addLog(String content) {
		// 写入txt文件
		try {
			
			bw.newLine();
			bw.write(content);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void closeLog() {

		try {
			fileWriter.flush();// 清空缓存
			bw.close();
			fileWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
