package cn.edu.njau.qing.util;

import java.sql.Connection;
import java.sql.SQLException;

//连接数据库
public class MyDBConn {
	String cos;
	String uname;
	String pwd;
	
	public MyDBConn(String host, String port, String dbname , String uname, String pwd){
		this.cos = "jdbc:mysql://"+ host +":"+port+"/"+ dbname +"?useUnicode=true&characterEncoding=utf8";
		this.uname = uname;
		this.pwd = pwd;
	}
	
	
	public Connection getConn() throws SQLException {
		try {
			Class.forName("com.mysql.jdbc.Driver");//加载Mysql驱动
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		Connection conn = java.sql.DriverManager.getConnection(cos,uname,pwd);
		return conn;
	}
}
