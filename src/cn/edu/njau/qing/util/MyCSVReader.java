package cn.edu.njau.qing.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import au.com.bytecode.opencsv.CSVReader;

//读取CSV文件
public class MyCSVReader {
	
	private String path;
	
	public MyCSVReader(String path){
		this.path = path;
	}
	
	public List<String[]> getLiCon(){
		List<String[]> fcontent = null;
		try {
			File f = new File(path);
			System.out.println("正在读取_"+ path +"_文件...");
			CSVReader cr = new CSVReader(new FileReader(f));
			fcontent  = cr.readAll();
		} catch (FileNotFoundException e) {
			System.out.println("CSV文件未找到！");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("CSV文件读取出错！");
			e.printStackTrace();
		}
		return fcontent;
	}
	
	public String[][] getStCon(){
		List<String[]> fcontent = null;
		try {
			File f = new File(path);
			System.out.println("正在读取_"+ path +"_文件...");
			CSVReader cr = new CSVReader(new FileReader(f));
			fcontent  = cr.readAll();
		} catch (FileNotFoundException e) {
			System.out.println("CSV文件未找到！");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("CSV文件读取出错！");
			e.printStackTrace();
		}
		int fcLen = fcontent.size();
		String[][] sc = new String[fcLen][];
		for(int i = 0; i < fcLen; i++){
			sc[i] = fcontent.get(i);
		}
		return sc;
	}
}
