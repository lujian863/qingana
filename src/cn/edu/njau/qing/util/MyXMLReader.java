package cn.edu.njau.qing.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

public class MyXMLReader {

	public String[][] DBConf = null;

	public MyXMLReader(String path) {

		SAXBuilder sb = new SAXBuilder();
		Document doc;
		try {
			doc = sb.build(new FileInputStream(path));

			Element root = doc.getRootElement();

			List<Element> nodes = new ArrayList<Element>();
			nodes = root.getChildren();
			int se_num = nodes.size();
			DBConf = new String[se_num][7];
			for (int i = 0; i < se_num; i++) {
				DBConf[i][0] = nodes.get(i).getChildText("host");
				DBConf[i][1] = nodes.get(i).getChildText("port");
				DBConf[i][2] = nodes.get(i).getChildText("dbname");
				DBConf[i][3] = nodes.get(i).getChildText("username");
				DBConf[i][4] = nodes.get(i).getChildText("password");
				DBConf[i][5] = nodes.get(i).getChildText("tablename");
				DBConf[i][6] = nodes.get(i).getChildText("fieldname");
			}

		} catch (FileNotFoundException e) {
			System.out.println("数据库配置文件未找到！");
			e.printStackTrace();
		} catch (JDOMException e) {
			System.out.println("数据库配置文件读取JDOM异常！");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("数据库配置文件读取出错！");
			e.printStackTrace();
		}
	}
}
