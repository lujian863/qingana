package cn.edu.njau.qing.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import au.com.bytecode.opencsv.CSVWriter;

public class MyCSVWriter {
	
	public static void writeCVS(String[][] sc, String fname) throws IOException {
		File tempFile = new File(fname);
		CSVWriter writer = new CSVWriter(new FileWriter(tempFile));
		for (int i = 0; i < sc.length; i++) {
			writer.writeNext(sc[i]);
		}
		writer.close();
	}
}
